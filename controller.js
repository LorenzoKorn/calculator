const PLUS = "+";
const MINUS = "-";
const TIMES = "*";
const DIVIDE = "/";
const MODULO = "%";
const multiplications = /(\((\-?\d+)[+|\-|\*|\/|%](\-?\d+)\)|((?!\())(\-?\d+)|(\-?\d+))\*(\((\-?\d+)[+|\-|\*|\/|%](\-?\d+)\)|(\-?\d+)((?!\)))|(\-?\d+))/g;
const divisions = /(\((\-?\d+)[+|\-|\*|\/|%](\-?\d+)\)|((?!\())(\-?\d+))\/(\((\-?\d)[+|\-|\*|\/|%](\-?\d+)\)|(\-?\d+)((?!\))))/g;
let usableString;

function submit() {
    let expression = document.getElementById("input").value;
    usableString = makeUsableString(expression);
    eval(expression);
    document.getElementById("calculation").innerText = evaluate(usableString);
}

function makeUsableString(expression) {
    expression = fixPlusMinus(expression);
    expression = group(expression, multiplications);
    expression = group(expression, divisions);
    return expression;
}

function group(expression, section) {
    let match;
    while ((match = section.exec(expression)) !== null) {
        expression = expression.replace(match[0], "(" + match[0] + ")");
        expression = fixPlusMinus(expression);
    }
    return expression;
}

function evaluate(expression) {
    expression = evaluateBrackets(expression);
    expression = removeBrackets(expression);
    expression = evaluateRest(expression);
    return expression;
}

function evaluateBrackets(expression) {
    let regex = /\((\-?\d+)[+|\-|\*|\/|%](\-?\d+)\)/g;
    let match;
    while ((match = regex.exec(expression)) !== null) {
        expression = expression.replace(match[0], calculate(match[0]));
        expression = fixPlusMinus(expression);
    }

    if (regex.exec(expression)) {
        expression = evaluateBrackets(expression);
    }

    return expression;
}

function evaluateRest(expression) {
    let regex = /(\-?\d+)\+(\-?\d+)/g;
    let match;
    while ((match = regex.exec(expression)) !== null) {
        expression = expression.replace(match[0], calculate(match[0]));
    }

    if (regex.exec(expression)) {
        expression = evaluateRest(expression);
    }
    return expression;
}

function calculate(section) {
    section = removeBrackets(section);
    let regex = /[+|\*|\/|%]/g;
    let numbers = section.split(regex);
    let operator = regex.exec(section)[0];
    let result;

    switch (operator) {
        case PLUS:
            result = add(numbers);
            break;
        case MINUS:
            result = subtract(numbers);
            break;
        case TIMES:
            result = multiply(numbers);
            break;
        case DIVIDE:
            result = divide(numbers);
            break;
        case MODULO:
            result = modulo(numbers);
            break;
    }

    return result < 0 ? "+" + result : result;
}

function fixPlusMinus(expression) {
    expression = expression.replace("-", "+-");
    expression = expression.replace("--", "+");
    expression = expression.replace(/\++/g, "+");
    expression = expression.replace("(+", "(");
    expression = expression.replace(")(", ")+(");
    return expression;
}

function removeBrackets(expression) {
    return expression.replace(/[\(|\)]/g, "");
}

function multiply(numbers) {
    return parseFloat(numbers[0]) * parseFloat(numbers[1]);
}

function divide(numbers) {
    return parseFloat(numbers[0]) / parseFloat(numbers[1]);
}

function add(numbers) {
    return parseFloat(numbers[0]) + parseFloat(numbers[1]);
}

function subtract(numbers) {
    return parseFloat(numbers[0]) - parseFloat(numbers[1]);
}

function modulo(numbers) {
    return parseFloat(numbers[0]) % parseFloat(numbers[1]);
}

// TODO 65/94+764-(89*6+945-87)*789 not working!